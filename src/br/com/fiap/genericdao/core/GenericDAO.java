package br.com.fiap.genericdao.core;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.fiap.genericdao.annotations.Column;
import br.com.fiap.genericdao.annotations.PrimaryKey;
import br.com.fiap.genericdao.annotations.Table;

public class GenericDAO<T> {
	private final Connection conexao;
	private final Class<T> classe;

	public GenericDAO(Connection conexao, Class<T> classe) {
		this.conexao = conexao;
		this.classe = classe;		
		validateClass();
	}
	
	public void add(T aAdicionar) throws RuntimeException{
		SqlQuery sql;
		try {
			sql = this.montaInsert();
			PreparedStatement stmt = this.carregaValores(sql, aAdicionar.getClass(), aAdicionar);			
			System.out.println(stmt);
			stmt.executeUpdate();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		}		
	}
	
	public void update(T aAtualizar) throws RuntimeException{
		try {
			SqlQuery sql = this.montaUpdate();
			PreparedStatement stmt;
			stmt = this.carregaValores(sql, aAtualizar.getClass(), aAtualizar);
			System.out.println(stmt);
			
			stmt.executeUpdate();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	} 
	
	public void remove(T aRemover) throws RuntimeException{
		try {
			SqlQuery sql = this.montaDelete();
			PreparedStatement stmt;
			stmt = this.carregaValores(sql, aRemover.getClass(), aRemover);
			System.out.println(stmt);
			stmt.executeUpdate();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	}
	
	public T findById(Number id) throws RuntimeException{
		try {
			SqlQuery sql = this.montaSelect(true);
			PreparedStatement prepareStatement;
			prepareStatement = this.conexao.prepareStatement(sql.toString());
			Field[] fields = classe.getDeclaredFields();
			for (int i = 0; i < fields.length; i++) {
				if(fields[i].isAnnotationPresent(PrimaryKey.class)){
					
					if (fields[i].getType().getName().equals("java.lang.Integer")) {
						prepareStatement.setInt(1, id.intValue());
					}else if (fields[i].getType().getName().equals("java.lang.Long")) {
						prepareStatement.setLong(1, id.longValue());
					}else if (fields[i].getType().getName().equals("java.lang.Short")) {
						prepareStatement.setShort(1, id.shortValue());
					}
					break;
				}
			}
			
			ResultSet rs = prepareStatement.executeQuery();
			System.out.println(prepareStatement);
			
			// TODO: criar valida��o para s� tentar montar o objeto caso tenha encontrado algum registro.
			rs.next();
			return montaObjeto(rs);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} catch (InstantiationException e) {
			throw new RuntimeException(e);
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	}
	
	public List<T> findAll() throws RuntimeException{
		try {
			SqlQuery sql = this.montaSelect(false);
			PreparedStatement prepareStatement;
			prepareStatement = this.conexao.prepareStatement(sql.toString());
			System.out.println(prepareStatement);
			ResultSet rs = prepareStatement.executeQuery();
			
			List<T> alunos = new ArrayList<T>();
			while(rs.next()){
				alunos.add(this.montaObjeto(rs));
			}
			
			return alunos;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} catch (InstantiationException e) {
			throw new RuntimeException(e);
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	} 
	
	private SqlQuery montaInsert() {
		Field[] fields = classe.getDeclaredFields();
		
		StringBuilder sql = new StringBuilder();
		sql.append("INSERT INTO");
		sql.append(" ");
		sql.append(classe.getAnnotation(Table.class).name());
		sql.append("");
		sql.append("(");
		
		StringBuilder campos = new StringBuilder();
		StringBuilder coringas = new StringBuilder();
		for(Field campo : fields){
			if(campo.isAnnotationPresent(PrimaryKey.class))
				continue;
			
			if(campo.isAnnotationPresent(Column.class)){
				campos.append(campo.getAnnotation(Column.class).name());
			}else{
				campos.append(campo.getName());
			}
			
			campos.append(", ");
			
			coringas.append("?");
			coringas.append(", ");
		}
		sql.append(campos.substring(0, campos.length() -2));
		sql.append(")");
		sql.append("VALUES(");
		sql.append(coringas.substring(0, coringas.length() -2));
		sql.append(")");

		return new SqlQuery(sql.toString(), SqlType.INSERT);
	}
	
	private SqlQuery montaUpdate(){
		Field[] fields = classe.getDeclaredFields();
		
		StringBuilder sql = new StringBuilder();
		sql.append("UPDATE ");
		sql.append(classe.getAnnotation(Table.class).name());
		sql.append(" SET ");
		
		StringBuilder campos = new StringBuilder();
		StringBuilder chave = new StringBuilder();
		for(Field campo : fields){
			String nomeCampo = "";
			if(campo.isAnnotationPresent(Column.class)){
				nomeCampo = campo.getAnnotation(Column.class).name();
			}else{
				nomeCampo = campo.getName();
			}
			
			if(campo.isAnnotationPresent(PrimaryKey.class)){
				chave.append(nomeCampo);
				chave.append(" = ");
				
				chave.append("?");
				continue;
			}
			campos.append(nomeCampo);
			campos.append(" = ");
			
			campos.append("?");
			campos.append(", ");
		}
		sql.append(campos.substring(0, campos.length() -2));
		sql.append(" WHERE ");
		sql.append(chave);		

		return new SqlQuery(sql.toString(), SqlType.UPDATE);
	} 
	
	private SqlQuery montaDelete(){
		Field[] fields = classe.getDeclaredFields();
		
		StringBuilder sql = new StringBuilder();
		sql.append("DELETE FROM ");
		sql.append(classe.getAnnotation(Table.class).name());
		
		StringBuilder chave = new StringBuilder();
		for(Field campo : fields){
			if(campo.isAnnotationPresent(PrimaryKey.class)){
				if(campo.isAnnotationPresent(Column.class)){
					chave.append(campo.getAnnotation(Column.class).name());
				}else{
					chave.append(campo.getName());
				}
				chave.append(" = ");
				
				chave.append("?");
				break;
			}
		}

		sql.append(" WHERE ");
		sql.append(chave);		

		return new SqlQuery(sql.toString(), SqlType.DELETE);
	} 
	
	private SqlQuery montaSelect(boolean byId){
		Field[] fields = classe.getDeclaredFields();
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT ");
		
		StringBuilder chave = new StringBuilder();
		StringBuilder campos = new StringBuilder();
		for(Field campo : fields){
			String nomeCampo = "";
			if(campo.isAnnotationPresent(Column.class)){
				nomeCampo = campo.getAnnotation(Column.class).name();
			}else{
				nomeCampo = campo.getName();
			}
			
			campos.append(nomeCampo);
			campos.append(", ");
			
			if(campo.isAnnotationPresent(PrimaryKey.class) && byId){
				chave.append(nomeCampo);
				chave.append(" = ");
				
				chave.append("?");
			}
		}
		sql.append(campos.substring(0, campos.length() -2));
		sql.append(" FROM ");
		sql.append(classe.getAnnotation(Table.class).name());
		
		if(byId){
			sql.append(" WHERE ");
			sql.append(chave);		
		}

		return new SqlQuery(sql.toString(), SqlType.SELECT);
	}	
	
	private PreparedStatement carregaValores(SqlQuery sql, Class<? extends Object> classe, T valores) throws SQLException, IllegalArgumentException, IllegalAccessException{
		Field[] fields = classe.getDeclaredFields();
		PreparedStatement prepareStatement = this.conexao.prepareStatement(sql.toString());
		int totalCampos = fields.length;
		int posicaoChavePrimaria = 0;
		for (int j = 0, i = 0; i < totalCampos; i++, j++) {
			if(fields[i].isAnnotationPresent(PrimaryKey.class)){
				if (sql.tipo == SqlType.INSERT) {
					j--;
					continue;
				}else if (sql.tipo == SqlType.UPDATE) {
					j--;
					posicaoChavePrimaria = i;
					continue;
				}else {
					posicaoChavePrimaria = i;
					break;
				}
			}
			
			if (sql.tipo == SqlType.INSERT || sql.tipo == SqlType.UPDATE) {
				this.setParametro(valores, fields[i], prepareStatement, j);
			}
		}
		
		if (sql.tipo != SqlType.INSERT && sql.tipo != SqlType.UPDATE) {
			this.setParametro(valores, fields[posicaoChavePrimaria], prepareStatement, 0);
		}else if(sql.tipo == SqlType.UPDATE){
			this.setParametro(valores, fields[posicaoChavePrimaria], prepareStatement, totalCampos-1);
		}

		return prepareStatement;
	}

	private void setParametro(T valores, Field campo, PreparedStatement prepareStatement, int j) throws IllegalAccessException, SQLException {
		campo.setAccessible(true);
		Object valorCampo = campo.get(valores);
		
		if(valorCampo instanceof String){
			prepareStatement.setString(j+1, (String)valorCampo);
		}else if(valorCampo instanceof Short){
			prepareStatement.setShort(j+1, (Short)valorCampo);
		}else if(valorCampo instanceof Long){
			prepareStatement.setLong(j+1, (Long)valorCampo);
		}else if(valorCampo instanceof Integer){
			prepareStatement.setInt(j+1, (Integer)valorCampo);
		}else if(valorCampo instanceof Boolean){
			prepareStatement.setBoolean(j+1, (Boolean)valorCampo);
		}else if(valorCampo instanceof Double){
			prepareStatement.setDouble(j+1, (Double)valorCampo);
		}else if(valorCampo instanceof Float){
			prepareStatement.setFloat(j+1, (Float)valorCampo);
		}else if(valorCampo instanceof Date){
			prepareStatement.setDate(j+1, new java.sql.Date(((Date)valorCampo).getTime()));
		}
		
		campo.setAccessible(false);
	}
	
	private void validateClass(){
		if(!classe.isAnnotationPresent(Table.class)){
			throw new IllegalArgumentException("Classe do objeto n�o � uma tabela.");
		}
	}
	
	private T montaObjeto(ResultSet rs) throws InstantiationException, IllegalAccessException, IllegalArgumentException, SQLException{
		T newInstance = classe.newInstance();
		Field[] campos = classe.getDeclaredFields();
		
		for(Field campo : campos){
			String nomeCampo = "";
			if(campo.isAnnotationPresent(Column.class)){
				nomeCampo = campo.getAnnotation(Column.class).name();
			}else{
				nomeCampo = campo.getName();
			}
			campo.setAccessible(true);
			//Object valorCampo = rs.getObject(nomeCampo);
			Class tipoCampo = campo.getType();
			
			if(tipoCampo.equals(String.class)){
				campo.set(newInstance, rs.getString(nomeCampo));
			}else if(tipoCampo.equals(Short.class)){
				campo.set(newInstance, rs.getShort(nomeCampo));
			}else if(tipoCampo.equals(Long.class)){
				campo.set(newInstance, rs.getLong(nomeCampo));
			}else if(tipoCampo.equals(Integer.class)){
				campo.set(newInstance, rs.getInt(nomeCampo));
			}else if(tipoCampo.equals(Boolean.class)){
				campo.set(newInstance, rs.getBoolean(nomeCampo));
			}else if(tipoCampo.equals(Double.class)){
				campo.set(newInstance, rs.getDouble(nomeCampo));
			}else if(tipoCampo.equals(Float.class)){
				campo.set(newInstance, rs.getFloat(nomeCampo));
			}else if(tipoCampo.equals(Date.class)){
				campo.set(newInstance, new Date(rs.getDate(nomeCampo).getTime()));
			}
			
			campo.setAccessible(false);
		}
		
		return newInstance;
	}
	
 	private enum SqlType{
		INSERT, UPDATE, DELETE, SELECT
	}
	
	private class SqlQuery{
		private final String sql;
		private final SqlType tipo;
		
		public SqlQuery(String sql, SqlType tipo){
			this.sql = sql;
			this.tipo = tipo;
		}
		
		@Override
		public String toString() {
			return sql;
		}
	}
}
